const Router       = require("express").Router;
const healthRouter = new Router();
healthRouter.get("/health", (req,res,next)=>{
    let server_time = new Date().toISOString();
    let uptime = process.uptime;
    res.json({
        server_time,
        uptime, 
        healthy:true
    })
})
module.exports = healthRouter;