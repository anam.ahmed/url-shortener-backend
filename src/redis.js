const Redis  = require("ioredis");
const config = require("./config"); 
const _r    = new Redis(config.REDIS, config.redisOptions);
_r.on("connect", ()=>console.info(`Redis Connected!`));
_r.on("error", ()=>console.info(`Redis Error!!`));
module.exports = _r;