const db = require("../models");
const _r = require("../redis");
module.exports = async function(job){
    await db.visit.addVisit(job.data.id);
    // Reset the visited list
    await _r.del(`visit_list`);
}