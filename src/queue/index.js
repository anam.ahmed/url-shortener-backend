const {Queue, Worker} = require("bullmq");
const connection      = require("../redis");
const visitSync       = require("./visit_sync_worker");
const visitQ          = new Queue("visits", {connection});
// Keeping the worker here for simplicity. since its not built to scale
const visitWorker     = new Worker("visits", visitSync, {connection});
visitWorker.on("completed", ()=>console.log(`Visit Synced`));
visitWorker.on("error", ()=>console.log(`Visit sync failed`));
function queueNewVisit(id){
    visitQ.add("urlvisit", {id});
}
module.exports = {
    visitQ,
    queueNewVisit
}