const {DataTypes} = require("sequelize");

module.exports = (db) =>{
    const Url = db.define("Url",{
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        url: {
            type: DataTypes.STRING,
            allowNull: false
        }
    });
    async function createShortened(url){
        return await Url.create({
            url
        });
    }
    async function findById(id){
        return await Url.findOne({where:{id}});
    }
    return {
        Url,
        createShortened,
        findById
    }
}




