function encode(num){
    return (1000+num).toString(16);
}

function decode(hexFormat){
    if(!hexFormat) return false;
    let decimal = parseInt(hexFormat, 16);
    if(decimal < 1000) return false;
    return decimal - 1000;
}

function validateURL(url){
    if(!url) return false;
    let ur = url.trim();
    if(!ur.match(/^https?:\/\/\S+$/)) return false; // yes, allow url like localhost too
    return ur;
}

function flatten(p){
    return new Promise((resolve,reject)=>{
        p.then(data=>resolve([data, null])).catch(e=>{
            console.log(e);
            return resolve([null,e]);
        });
    });
}

module.exports = {
    decode,
    encode,
    validateURL,
    flatten
}