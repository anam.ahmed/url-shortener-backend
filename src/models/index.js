const {Sequelize, QueryTypes} = require("sequelize");
const utils                   = require("../utils");
const config                  = require("../config");
const db = new Sequelize(config.DATABASE_URL, config.dbOptions);
db.authenticate()
    .then(auth=>console.log("DB Connected"))
    .catch(err=>console.log("DB Error"));
// Models
const url         = require("./urls")(db);
const visit       = require("./visits")(db);

// Relationships
url.Url.hasMany(visit.Visit, {foreignKey: {name: "url_id"}});
visit.Visit.belongsTo(url.Url);
// Helpers
async function getVisits(){
    /**Thought I'd show some raw SQL skills here */
    /**Besides, Raw join queries are usually faster than query builder join queries */
    let [_visits] = await db.query(`
    SELECT "Urls"."id", "Urls"."url", count("Visits"."id")
    AS total_visit 
    FROM "Urls" RIGHT JOIN "Visits" 
    ON "Visits"."url_id" = "Urls"."id" 
    GROUP BY "Urls"."id"
    ORDER BY "total_visit" DESC
    LIMIT 100;
    `);
    if(!_visits?.length) return [];
    return _visits.map(_v=>{
        return {
            ..._v,
            alias: utils.encode(_v.id)
        }
    });
}
// url.createShortened("https://googlr.com");
// visit.addVisit(2);
// getVisits().then(v=>console.log(v));
module.exports =  {
    db,
    url,
    visit,
    helpers: {
        getVisits
    }
}