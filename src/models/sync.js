const db = require(".");
db.db.sync({force: true}).then(d=>{
    console.log(d);
    console.log("Database synced");
    process.exit(0);
}).catch(e=>{
    console.error("Database sync failed");
    console.log(e);
});