const express         = require("express");
const config          = require("./config");
const app             = express();
const cors            = require("cors");
// Middlewares
app.use(express.json());
app.use(cors({origin:config.CORS_ORIGIN}));
// Controllers
const urlController    = require("./controllers/urlController");
const visitController  = require("./controllers/visitController"); 
const healthController = require("./controllers/healthController"); 
const errorHandler     = require("./controllers/error");
// Register controllers
app.use(urlController);
app.use(healthController);
app.use(visitController);
// Global error handler
app.use(errorHandler);
app.listen(config.PORT, ()=>console.info(`Listening on: ${config.PORT}`));