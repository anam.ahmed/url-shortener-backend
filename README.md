## URL Shortener backend

URL shortener REST API. Technology stack: Node, Express, Postgres, Redis

### How to Run
The application is packed in a container, you don't need to install any additional dependency to run this. You only need a container runtime, in this case, Docker.  

1. Install [docker](https://docs.docker.com/engine/install/) (v20 or later)
2. Navigate to the application folder from command line.
3.  Check the `.env` file, modify the values if needed
4. run `docker compose up` and grab a cup of coffee.
5. by the time you are done drinking the coffee, the application hould be up and running.
6. Look for the console output similar to: `Listening on: 3030` to verify that the build is finished.
7. If you are running the application for the first time, run `docker compose exec app node /opt/app/src/models/sync.js` to create the database tables.
8. Verify application status by visiting this URL: <http://localhost:3030/health> (if you've changed port number in .env use that port number instead)

### API documentation
You will find the [Insomnia](https://insomnia.rest/) collection in the repository as `api.collection.json`. here's a short description anyways:

the base URL is the application URL (e.g: `http://localhost:3030`). All incoming and outgoing data is of type `Application/JSON`.

|Endpoint|Description|Request Type|Params| usage |
|---------|------------|------|------| ---- |
|/add|Shorten a new URL|POST| url (mandatory)| returns `alias` and `base` in the response, the full shortened URL will be `base/alias` e.g: http://localhost:3030/3e5|
|/list|List 100 most visited URL|GET|N/A| Renders the list page in the client|
|/health| Check server health and uptime| GET | N/A | used by client to wake up backend application, helps with frontend performance|

### Security
This API is developed without any security measure such as authentication just to keep things simple.


Thanks for reading this documentation.