FROM node:15-alpine

WORKDIR /opt/app
COPY ./package.json .
RUN yarn
COPY ./src ./src
CMD ["yarn", "start"]
