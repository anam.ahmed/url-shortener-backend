const {DataTypes} = require("sequelize");

module.exports = (db)=>{
    const Visit = db.define("Visit",{
        id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        url_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        }
    });
    async function addVisit(url_id){
        return await Visit.create({
            url_id
        });
    }
    return {
        Visit,
        addVisit
    }
}
