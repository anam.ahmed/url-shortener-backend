const {
    DATABASE_URL,
    REDIS_URL,
    REDISCLOUD_URL,
    BASE_URL = "http://localhost:3030", 
    PORT = 3030,
    IS_HEROKU = "false",
    CORS_ORIGIN = "*"
} = process.env;

let base  = BASE_URL;
let REDIS = REDISCLOUD_URL || REDIS_URL;

let dbOptions    = {dialect: "postgres"}
let redisOptions = {}

if(IS_HEROKU === "true"){
    dbOptions = {
        ...dbOptions,
        dialectOptions:{
            ssl: {
                require: true,
                rejectUnauthorized: false
            }
        }
    }
    redisOptions = {enableReadyCheck: false}
}


module.exports = {
    DATABASE_URL,
    dbOptions,
    REDIS,
    redisOptions,
    BASE_URL,
    base,
    PORT,
    CORS_ORIGIN
}