const Router      = require("express").Router;
const visitRouter = new Router();
const db          = require("../models");
const _r          = require("../redis");
const queue       = require("../queue");
const utils       = require("../utils");
const config      = require("../config");
visitRouter.get("/list", async (req,res,next) =>{
    // cache the list
    // ideally I'd reset it in 5 minutes
    // but to get instant action, I'll reset it with every visit
    let cached = await _r.get(`visit_list`);
    if(cached) return res.header({"Content-type": "Application/JSON"}).end(cached);
    let data = await db.helpers.getVisits();
    if(!data) return next("Could not load visit data");
    await _r.set(`visit_list`, JSON.stringify({error: false, data, base:config.base}));
    res.json({error:false, data, base:config.base});
});
visitRouter.get("/:alias",async (req,res,next)=>{
    let id = utils.decode(req.params.alias);
    console.log(id);
    if(!id) return next("No record found");
    let cached = await _r.get(`url_${id}`);
    if(cached){
        console.log(`Serving from cache`);
        queue.queueNewVisit(id);
        return res.redirect(302, cached);
    }
    let dbData = await db.url.findById(id);
    if(!dbData) return next("No record found");
    await _r.set(`url_${id}`, dbData.dataValues.url);
    // console.log(dbData.dataValues.url);
    queue.queueNewVisit(id);
    res.redirect(302, dbData.dataValues.url);
});
module.exports = visitRouter;