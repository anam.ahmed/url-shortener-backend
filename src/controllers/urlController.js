const Router    = require("express").Router;
const urlRouter = new Router();
const {url}     = require("../models");
const utils     = require("../utils");
const config    = require("../config");
urlRouter.post("/add",async (req,res,next)=>{
    let validUrl = utils.validateURL(req.body.url);
    if(!validUrl) return next("URL is not valid");
    let recorded = await url.createShortened(validUrl);
    if(!recorded) return next("Could not create shortened URL");
    let alias = utils.encode(recorded.dataValues.id); 
    res.json({
        error: false,
        alias,
        url: validUrl,
        base: config.base
    });
});

module.exports = urlRouter;