module.exports = function(err, req, res, next){
    let message = err?.message ?? err ?? "Unknown server error";
    res.status(500).json({error:true, message});
}